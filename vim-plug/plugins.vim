" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " File Explorer
    Plug 'scrooloose/NERDTree'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Stable version of coc
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    " " Keeping up to date with master
    Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}
    "Themes
    Plug 'joshdick/onedark.vim'
    "snippets
    " Plug 'honza/vim-snippets'
    Plug 'sirver/ultisnips'
    "FZF
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'airblade/vim-rooter' 
    "Busqueda de archivos
    Plug 'easymotion/vim-easymotion'
    "Para comentar o descomentar lineas
    Plug 'scrooloose/nerdcommenter'
    "Para colocar el tagbar
    Plug 'preservim/tagbar'
    "Iconos de nerdtree
    Plug 'ryanoasis/vim-devicons'
    "lightline status bar
    Plug 'itchyny/lightline.vim'
    Plug 'maximbaz/lightline-ale'
    Plug 'itchyny/landscape.vim' 
    "Asynchronous Lint Engine 
    Plug 'dense-analysis/ale'
    Plug 'Vimjas/vim-python-pep8-indent'
    "indentline
    Plug 'yggdroot/indentline'
    "in analogy with visual-block
    Plug 'mg979/vim-visual-multi', {'branch': 'master'}
    "typing
    Plug 'alvan/vim-closetag'
    Plug 'tpope/vim-surround'
    "Comandos para el manejo de selección de copiado
    Plug 'machakann/vim-highlightedyank'
    "visualizar los ctags
    Plug 'liuchengxu/vista.vim'
    "Adding color with colorizer
    Plug 'norcalli/nvim-colorizer.lua'
    "Adding color with rainbow parentheses
    Plug 'junegunn/rainbow_parentheses.vim'    


call plug#end()
