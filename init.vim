so ~/.config/nvim/general/settings.vim
so ~/.config/nvim/vim-plug/plugins.vim
so ~/.config/nvim/themes/onedark.vim
so ~/.config/nvim/keys/mappings.vim
so ~/.config/nvim/plug-config/fzf.vim
so ~/.config/nvim/plug-config/coc.vim
so ~/.config/nvim/plug-config/ultisnippets.vim
so ~/.config/nvim/plug-config/nerdtree.vim
so ~/.config/nvim/plug-config/nerdcommenter.vim
so ~/.config/nvim/plug-config/lightline.vim
so ~/.config/nvim/plug-config/ale.vim
so ~/.config/nvim/plug-config/indentline.vim
so ~/.config/nvim/plug-config/closetag.vim
so ~/.config/nvim/plug-config/vista.vim
so ~/.config/nvim/plug-config/vim-visual.vim          "para reemplazar C-n por C-d en vim-visual-multi"
" so ~/.config/nvim/plug-config/vim_fugitive.vim
luafile ~/.config/nvim/lua/plug-colorizer.lua
so ~/.config/nvim/plug-config/rainbow-parentheses.vim









"para el uso con vim y neovim
"set runtimepath^=~/.vim runtimepath+=~/.vim/after
"let &packpath=&runtimepath
"source ~/.vimr:
"source $HOME/.config/nvim/general/settings.vm
