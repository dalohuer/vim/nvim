"Configuraciones NerdTree
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1 "para que el NerdTree se cierre cada vez que se abra un archivo
let NERDTreeAutoDeleteBuffer=1
let NERDTreeMinimalUI=1
let NERDTreeDirArrows=1
let NERDTreeShowLineNumbers=1
let NERDTreeMapOpenInTab='\t'
