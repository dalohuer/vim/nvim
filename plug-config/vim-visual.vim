 " let g:VM_default_mappings = 0
"let g:VM_maps = {}
" let g:VM_maps['Find Under']         = '<C-d>'           " replace C-n
" let g:VM_maps['Find Subword Under'] = '<C-d>'           " replace visual C-n
" let g:VM_mouse_mappings = 1
" nmap   <C-LeftMouse>         <Plug>(VM-Mouse-Cursor)
" nmap   <C-RightMouse>        <Plug>(VM-Mouse-Word)
" nmap   <M-C-RightMouse>      <Plug>(VM-Mouse-Column)
